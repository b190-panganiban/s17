/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:


function userDetails(){
	let fullName = prompt("What is your full name?");
    let yourAge = prompt("How old are you?");
    let yourLocation = prompt("Where do you live?");
    console.log("Hello, " + fullName);
    console.log("You are " + yourAge + " years old.");
    console.log("You live in " + yourLocation);

};
userDetails();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:


function getMusicalArtist(){
  console.log("1. Taylor Swift");
 console.log("2. Ariana Grande");
 console.log("3. Billie Eillis");
 console.log("4. Justin Bieber");
 console.log("5. Frank Sinatra");
};
getMusicalArtist();


/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

function favoriteMovies(){
  console.log("1. A walk to remember");
  console.log("Rotten Tomatoes rating: 97%");
  console.log("2. The notebook");
  console.log("Rotten Tomatoes rating: 98%");
  console.log("3. The fault in our stars");
  console.log("Rotten Tomatoes rating: 95%");
  console.log("4. Midnight Sun");
  console.log("Rotten Tomatoes rating: 94%");
  console.log("5. Stranger things");
 console.log("Rotten Tomatoes rating: 97%");
};
favoriteMovies();



/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers (){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();

